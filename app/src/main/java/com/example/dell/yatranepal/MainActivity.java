package com.example.dell.yatranepal;

import android.graphics.drawable.AnimationDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout myLayout;
    AnimationDrawable animationDrawable;
    EditText etUser, etPass;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
          myLayout = (RelativeLayout) findViewById(R.id.myLayout);

        animationDrawable = (AnimationDrawable) myLayout.getBackground();
        animationDrawable.setEnterFadeDuration(4500);
        animationDrawable.setExitFadeDuration(4500);

        animationDrawable.start();


    }

    private void init() {
        etUser = findViewById(R.id.etUser);
        etPass = findViewById(R.id.etPass);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener((View.OnClickListener) this);

    }

    @Override
    public void onClick(View v) {

        String uName = etUser.getText().toString();
        String psw = etPass.getText().toString();
        if (uName.equals("") && psw.equals("")) {
            Toast.makeText(this, "Both Fields Cannot Be Empty", Toast.LENGTH_SHORT);
        } else if (uName.equals(uName) || psw.equals(psw)) {
            Toast.makeText(this, "Login Recorded", Toast.LENGTH_SHORT);
        } else if (uName.equals(uName) && psw.equals("")) {
            Toast.makeText(this, "Password Cannot Be Empty", Toast.LENGTH_SHORT);
        } else {
            Toast.makeText(this, "Username Cannot Be Empty", Toast.LENGTH_SHORT);
        }
     finish();

    }
}
